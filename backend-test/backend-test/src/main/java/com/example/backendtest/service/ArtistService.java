package com.example.backendtest.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.backendtest.Entity.Artist;
import com.example.backendtest.Repository.ArtistRepository;

@Service
public class ArtistService {

    @Autowired
    ArtistRepository repository;

    public Page<Artist> search(
            String searchTerm,
            int page,
            int size) {
        PageRequest pageRequest = PageRequest.of(
                page,
                size,
                Sort.Direction.ASC,
                "firstName");

        return repository.search(
                searchTerm.toLowerCase(),
                pageRequest);
    }

    public Page<Artist> findAll() {
        int page = 0;
        int size = 10;
        PageRequest pageRequest = PageRequest.of(
                page,
                size,
                Sort.Direction.ASC,
                "firstName");
        return new PageImpl<>(
                repository.findAll(), 
                pageRequest, size);
    }

}