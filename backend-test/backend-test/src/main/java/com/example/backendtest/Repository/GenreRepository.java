package com.example.backendtest.Repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.backendtest.Entity.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {
    @Query("FROM Genre g WHERE LOWER(g.description) like %:search% ")
    Page<Genre> search(
            @Param("search") String search,Pageable pageable);
}