package com.example.backendtest.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.backendtest.Entity.Genre;
import com.example.backendtest.Repository.GenreRepository;
import com.example.backendtest.service.GenreService;

@RestController
public class GenreController {
    @Autowired
    private GenreRepository GenresRepository;
    
    @Autowired
    private GenreService service;
    
    @RequestMapping(value = "/genres", method = RequestMethod.GET)
    public Page<Genre> Get(
    		@RequestParam(value="page",required = false,defaultValue="0")Integer page,
    		@RequestParam(value="size",required=false,defaultValue="10")Integer size,
    		@RequestParam(value="search",required=false,defaultValue="")String search) {
    	return service.search(search,page,size);
    }

    @RequestMapping(value = "/genres", method =  RequestMethod.POST)
    public Genre Post(@Valid @RequestBody Genre genre)
    {
    	Date date = new Date();
      	String formato = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat formatador = new SimpleDateFormat(formato);
        String formatada = formatador.format(date);
    	genre.setCreatedAt(""+formatada);
        return GenresRepository.save(genre);
    }

    @RequestMapping(value = "/genres/{id}", method = RequestMethod.GET)
    public ResponseEntity<Genre> GetById(@PathVariable(value = "id") long id)
    {
        Optional<Genre> genre = GenresRepository.findById(id);
        if(genre.isPresent())
            return new ResponseEntity<Genre>(genre.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    
    @RequestMapping(value = "/genres/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Genre> Put(@PathVariable(value = "id") long id, @Valid @RequestBody Genre newGenre)
    {
        Optional<Genre> oldGenre = GenresRepository.findById(id);
        if(oldGenre.isPresent()){
            Genre genre = oldGenre.get();
            if(newGenre.getDescription() != null) {
            	genre.setDescription(newGenre.getDescription());
            }            
            Date date = new Date();
            	String formato = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat formatador = new SimpleDateFormat(formato);
                String formatada = formatador.format(date);
            	genre.setUpdatedAt("" + formatada);
            GenresRepository.save(genre);
            return new ResponseEntity<Genre>(genre, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

//    @RequestMapping(value = "/genres/{id}", method = RequestMethod.DELETE)
//    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id)
//    {
//        Optional<Genre> genre = GenresRepository.findById(id);
//        if(genre.isPresent()){
//            GenresRepository.delete(genre.get());
//            return new ResponseEntity<>(HttpStatus.OK);
//        }
//        else
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }
    
    
}