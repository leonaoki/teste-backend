package com.example.backendtest.Entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;


@Entity
public class Movie
{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String title;
    
    @Column
    private int releaseYear;
    
    @ManyToMany(mappedBy = "GenreMovie")
    private Set<Genre> Genres;
    
	@ManyToMany(mappedBy = "ArtistMovie")
    private Set<Artist> Artists;
    
	@ManyToOne
	@JoinColumn
    private Artist director;
    
    @Column
    private String createdAt;
    
    @Column
    private String updatedAt;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getReleaseYear() {
		return releaseYear;
	}
	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}
	public Artist getDirector() {
		return director;
	}
	public void setDirector(Artist director) {
		this.director = director;
	}
	
	public Set<Genre> getGenres() {
		return Genres;
	}
	public void setGenres(Set<Genre> genres) {
		Genres = genres;
	}
	public Set<Artist> getArtists() {
		return Artists;
	}
	public void setArtists(Set<Artist> artists) {
		Artists = artists;
	}

	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
   
 
}