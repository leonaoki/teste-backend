package com.example.backendtest.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.backendtest.Entity.Movie;
import com.example.backendtest.Repository.MovieRepository;
import com.example.backendtest.service.MovieService;

@RestController
public class MovieController {
    @Autowired
    private MovieRepository MovieRepository;

    @Autowired
    private MovieService service;

    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    public Page<Movie> Get(
    		@RequestParam(value="page",required = false,defaultValue="0")Integer page,
    		@RequestParam(value="size",required=false,defaultValue="10")Integer size,
    		@RequestParam(value="search",required=false,defaultValue="")String search) {
    	return service.search(search,page,size);
    }

    @RequestMapping(value = "/movies", method =  RequestMethod.POST)
    public Movie Post(@Valid @RequestBody Movie movie)
    {
    	Date date = new Date();
      	String formato = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat formatador = new SimpleDateFormat(formato);
        String formatada = formatador.format(date);
    	movie.setCreatedAt(""+formatada);
        return MovieRepository.save(movie);
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.GET)
    public ResponseEntity<Movie> GetById(@PathVariable(value = "id") long id)
    {
        Optional<Movie> movie = MovieRepository.findById(id);
        if(movie.isPresent())
            return new ResponseEntity<Movie>(movie.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    
    @RequestMapping(value = "/movies/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Movie> Put(@PathVariable(value = "id") long id, @Valid @RequestBody Movie newMovie)
    {
        Optional<Movie> oldMovie = MovieRepository.findById(id);
        if(oldMovie.isPresent()){
            Movie movie = oldMovie.get();
            if(newMovie.getTitle() != null) {
            	movie.setTitle(newMovie.getTitle());
            }
            if(newMovie.getTitle() != null) {
            	movie.setReleaseYear(newMovie.getReleaseYear());
            }
            Date date = new Date();
            	String formato = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat formatador = new SimpleDateFormat(formato);
                String formatada = formatador.format(date);
            	movie.setUpdatedAt("" + formatada);
            MovieRepository.save(movie);
            return new ResponseEntity<Movie>(movie, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id)
    {
        Optional<Movie> movie = MovieRepository.findById(id);
        if(movie.isPresent()){
            MovieRepository.delete(movie.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    
}