package com.example.backendtest.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.backendtest.Entity.Artist;
import com.example.backendtest.Entity.Movie;
import com.example.backendtest.Repository.ArtistRepository;
import com.example.backendtest.Repository.MovieRepository;
import com.example.backendtest.service.ArtistService;

@RestController
public class ArtistController {
    @Autowired
    private ArtistRepository ArtistRepository;
    @Autowired
    private MovieRepository MovieRepository;
    @Autowired
    private ArtistService service;

    @RequestMapping(value = "/artists", method = RequestMethod.GET)
    public Page<Artist> Get(
    		@RequestParam(value="page",required = false,defaultValue="0")Integer page,
    		@RequestParam(value="size",required=false,defaultValue="10")Integer size,
    		@RequestParam(value="search",required=false,defaultValue="")String search) {
    	return service.search(search,page,size);
    }

    @RequestMapping(value = "/artists", method =  RequestMethod.POST)
    public Artist Post(@Valid @RequestBody Artist artist)
    {
    	Date date = new Date();
      	String formato = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat formatador = new SimpleDateFormat(formato);
        String formatada = formatador.format(date);
    	artist.setCreatedAt(""+formatada);
        return ArtistRepository.save(artist);
    }

    @RequestMapping(value = "/artists/{id}", method = RequestMethod.GET)
    public ResponseEntity<Artist> GetById(@PathVariable(value = "id") long id)
    {
        Optional<Artist> artist = ArtistRepository.findById(id);
        if(artist.isPresent())
            return new ResponseEntity<Artist>(artist.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    
    @RequestMapping(value = "/artists/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Artist> Put(@PathVariable(value = "id") long id, @Valid @RequestBody Artist newArtist)
    {
        Optional<Artist> oldArtist = ArtistRepository.findById(id);
        if(oldArtist.isPresent()){
            Artist artist = oldArtist.get();
            if(newArtist.getFirstName() != null) {
            	artist.setFirstName(newArtist.getFirstName());
            }
            if(newArtist.getLastName() != null) {
            	artist.setLastName(newArtist.getLastName());
            }
            if(newArtist.getDateOfBirth() != null) {
            	artist.setDateOfBirth(newArtist.getDateOfBirth());
            }
            Date date = new Date();
            	String formato = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat formatador = new SimpleDateFormat(formato);
                String formatada = formatador.format(date);
            	artist.setUpdatedAt("" + formatada);
            ArtistRepository.save(artist);
            return new ResponseEntity<Artist>(artist, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @RequestMapping(value = "/artists/{id}/filmography", method =  RequestMethod.GET)
    public List<Movie> Get(@PathVariable(value = "id") long id)
    {
        List<Movie> movie = MovieRepository.findAll();
        List<Movie> result = new ArrayList<Movie>();
         for(Movie movies: movie) {
        		 Set<Artist> artist = movies.getArtists();
        		 for(Artist artists : artist) {
        			 if(artists.getId() == id) {
        				 result.add(movies);
        			 }
        		 }
        		 if(movies.getDirector().getId() == id) {
        			 result.add(movies);
        		 }
         }
         
         return result;
         
    }

//    @RequestMapping(value = "/artists/{id}", method = RequestMethod.DELETE)
//    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id)
//    {
//        Optional<Artist> artist = ArtistRepository.findById(id);
//        if(artist.isPresent()){
//            ArtistRepository.delete(artist.get());
//            return new ResponseEntity<>(HttpStatus.OK);
//        }
//        else
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }
    
    
}