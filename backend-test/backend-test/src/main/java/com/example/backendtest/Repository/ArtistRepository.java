package com.example.backendtest.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.backendtest.Entity.Artist;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Long> {
	@Query("FROM Artist a " +
	           "WHERE LOWER(a.firstName) like %:search% " +
	           "OR LOWER(a.lastName) like %:search%")
	    Page<Artist> search(
	            @Param("search") String search, 
	            Pageable pageable);
}