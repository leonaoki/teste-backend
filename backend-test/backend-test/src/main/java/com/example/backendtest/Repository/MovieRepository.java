package com.example.backendtest.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.backendtest.Entity.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> { 
	@Query("FROM Movie m " +
	           "WHERE LOWER(m.title) like %:search% ")
	    Page<Movie> search(
	            @Param("search") String search, 
	            Pageable pageable);
}