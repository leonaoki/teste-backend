package com.example.backendtest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.backendtest.Entity.Genre;
import com.example.backendtest.Repository.GenreRepository;

@Service
public class GenreService {

    @Autowired
    GenreRepository repository;

    public Page<Genre> search(
            String searchTerm,
            int page,
            int size) {
        PageRequest pageRequest = PageRequest.of(
                page,
                size,
                Sort.Direction.ASC,
                "description");

        return repository.search(
                searchTerm.toLowerCase(),
                pageRequest);
    }

    public Page<Genre> findAll() {
        int page = 0;
        int size = 10;
        PageRequest pageRequest = PageRequest.of(
                page,
                size,
                Sort.Direction.ASC,
                "description");
        return new PageImpl<>(
                repository.findAll(), 
                pageRequest, size);
    }

}